package yan.yashyn.barber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.request.TimeTableForBarberRequest;
import yan.yashyn.barber.dto.response.TimeTableForBarberResponse;
import yan.yashyn.barber.entity.TimeTableForBarber;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.DayRepository;
import yan.yashyn.barber.repository.TimeTableForBarberRepository;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimeTableForBarberService {
    @Autowired
    private TimeTableForBarberRepository timeTableForBarberRepository;
    @Autowired
    private DayRepository dayRepository;

    public TimeTableForBarberResponse set (TimeTableForBarberRequest timeTableForBarberRequest) throws WrongInputException {
        TimeTableForBarber timeTableForBarber = new TimeTableForBarber();
        timeTableForBarber.setFreeHour(timeTableForBarberRequest.getFreeHour());
        timeTableForBarber.setHour(timeTableForBarberRequest.getTime());
        timeTableForBarber.setDay(dayRepository.getOne(timeTableForBarberRequest.getDateId()));
        if ((findByHourAndDayId(timeTableForBarberRequest.getTime(), timeTableForBarberRequest.getDateId())) == null){
            return new TimeTableForBarberResponse(timeTableForBarberRepository.save(timeTableForBarber));
        }else {
            return findByHourAndDayId (timeTableForBarberRequest.getTime(), timeTableForBarberRequest.getDateId());
        }

    }

    public TimeTableForBarberResponse findByHourAndDayId (LocalTime hour, Long day_id){
        return timeTableForBarberRepository.findByHourAndDay_Id(hour, day_id);
    }

    public List<TimeTableForBarberResponse> getByDay(Long dayId) {
        List<TimeTableForBarberResponse> result = timeTableForBarberRepository.findByDay_IdAndFreeHourEquals(dayId, true)
                .stream().sorted(Comparator.comparing(TimeTableForBarberResponse::getTime)).collect(Collectors.toList());
        return result;
    }

    public TimeTableForBarber findOneById(Long ttfbId) throws WrongInputException {
        return timeTableForBarberRepository.findById(ttfbId).orElseThrow(() -> new WrongInputException("Hour wasn't found"));
    }
}
