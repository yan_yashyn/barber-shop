package yan.yashyn.barber.dto.response;

import lombok.Getter;
import lombok.Setter;
import yan.yashyn.barber.entity.Order;
import yan.yashyn.barber.entity.StatusConsts;

import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
public class OrderResponse {

    private Long id;

    private String name;

    private String surname;

    private String phoneNumber;

    @Email
    private String email;

    private StatusConsts status;

    private String barberName;

    private String barberSurname;

    private String barberRang;

    private LocalDate day;

    private LocalTime hour;

    private String service;

    private Integer price;

    public OrderResponse (Order order){
        id = order.getId();
        name = order.getName();
        surname = order.getSurname();
        phoneNumber = order.getPhoneNumber();
        email = order.getEmail();
        status = order.getStatus();
        barberName = order.getBarber().getName();
        barberSurname = order.getBarber().getSurname();
        barberRang = order.getBarber().getRang().getRang();
        day = order.getDay().getDay();
        hour = order.getTimeTableForBarber().getHour();
        service = order.getService().getService();
        price = order.getService().getPrice();
    }
}
