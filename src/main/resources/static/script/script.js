var mainUrl = "http://localhost:8080";

setModalConfiguration();
getRangs();
setActionOnSubmitButton();
document.getElementById("rangDrop").addEventListener("input", getBarbers)
document.getElementById("rangDrop").addEventListener("input", getServices)
document.getElementById("barberDrop").addEventListener("input", getDays)
document.getElementById("dayDrop").addEventListener("input", getHours)



function setActionOnSubmitButton() {
    $("#submitOrder").click(function () {
        var name = $('#name').val();
        var surname = $('#surname').val();
        var phoneNumber = $('#phoneNumber').val();
        var email = $('#email').val();
        var barberId = $('#barberDrop').val();
        var dayId = $('#dayDrop').val();
        var ttfbId = $('#hoursDrop').val();
        var serviceId = $('#serviceDrop').val();

        var newOrder = {
            "barberId": barberId,
            "dayId": dayId,
            "email": email,
            "name": name,
            "phoneNumber": phoneNumber,
            "serviceId": serviceId,
            "status": "WAITING",
            "surname": surname,
            "timeTableForBarberId": ttfbId
        };

        $.ajax({
            url: mainUrl + "/order/create",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(newOrder),
            success: function (data) {
                location.reload();
            },
            error: function (error) {
                alert(error.message);
            }
        });


    })

}

function setModalConfiguration() {
    // Get the modal
    var modal = document.getElementById('bookingWindow');

    // Get the button that opens the modal
    var btn = document.getElementById("bookOnline");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function () {
        modal.style.display = "block";
    };

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };

}

function getRangs() {
    $.ajax({
        url: mainUrl + "/rang/findall",
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            setRangs(dataResponse);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setRangs(rangs) {
    let list = $("#rangDrop");
    for (let rang of rangs) {
        list.append(`<option value="` + rang.id + `">` + rang.rangName + `</option>`);
    }
}

function getBarbers (){
    $.ajax({
        url: mainUrl + "/barber/byrang?rangId=" + $("#rangDrop").val(),
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            setBarbers(dataResponse);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setBarbers(barbers) {
    let list = $("#barberDrop");
    list.html("");
    for (let barber of barbers) {
        list.append(`<option value="` + barber.id + `">` + barber.name + " " + barber.surname +`</option>`);
    }
}

function getServices() {
    $.ajax({
        url: mainUrl + "/service/byrang?rangId=" + $("#rangDrop").val(),
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            setServices(dataResponse);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setServices(services) {
    let list = $("#serviceDrop");
    list.html("");
    for (let service of services) {
        list.append(`<option value="` + service.id + `">` + service.serviceName + " " + service.price + " ₴" + `</option>`);
    }
}

function getDays() {
    $.ajax({
        url: mainUrl + "/day/get?barberId=" + $("#barberDrop").val(),
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            setDays(dataResponse);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setDays(days) {
    let list = $("#dayDrop");
    list.html("");
    for (let day of days) {
        list.append(`<option value="` + day.id + `">` + day.date + `</option>`);
    }
}

function getHours() {
    $.ajax({
        url: mainUrl + "/ttforbarber/getbyday?dayId=" + $("#dayDrop").val(),
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            setHours(dataResponse);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setHours(hours) {
    let list = $("#hoursDrop");
    list.html("");
    for (let hour of hours) {
        list.append(`<option value="` + hour.id + `">` + hour.time + `</option>`);
    }
}