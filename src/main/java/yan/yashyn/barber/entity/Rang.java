package yan.yashyn.barber.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Rang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String rang;

    @OneToMany (mappedBy = "rang")
    private List <Barber> barbers = new ArrayList<>();

    @Override
    public String toString() {
        return "Rang{" +
                "id=" + id +
                ", rang='" + rang + '\'' +
                '}';
    }
}
