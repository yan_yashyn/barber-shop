package yan.yashyn.barber.dto.request;

import lombok.Getter;
import lombok.Setter;
import yan.yashyn.barber.entity.StatusConsts;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Getter
@Setter
public class OrderRequest {

    private String name;

    private String surname;

    private String phoneNumber;

    @Email
    private String email;

    private StatusConsts status;

    private Long barberId;

    private Long dayId;

    private Long timeTableForBarberId;

    private Long serviceId;
}
