package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.request.TimeTableForBarberRequest;
import yan.yashyn.barber.dto.response.TimeTableForBarberResponse;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.TimeTableForBarberRepository;
import yan.yashyn.barber.service.TimeTableForBarberService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/ttforbarber")
public class TimeTableForBarberController {
    @Autowired
    private TimeTableForBarberService timeTableForBarberService;

    @PostMapping("/set")
    public TimeTableForBarberResponse set(@RequestBody TimeTableForBarberRequest timeTableForBarberRequest) throws WrongInputException {
        return timeTableForBarberService.set(timeTableForBarberRequest);
    }

    @GetMapping("/getbyday")
    public List<TimeTableForBarberResponse> getByDay (@RequestParam Long dayId) {
        return timeTableForBarberService.getByDay(dayId);
    }
}
