package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.request.OrderRequest;
import yan.yashyn.barber.dto.response.OrderResponse;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.service.OrderService;

@CrossOrigin
@RestController
@RequestMapping("/order")
public class OrderController{

    @Autowired
    private OrderService orderService;

    @PostMapping("/create")
    public OrderResponse create (@RequestBody OrderRequest orderRequest) throws WrongInputException {
        return orderService.create(orderRequest);
    }
}
