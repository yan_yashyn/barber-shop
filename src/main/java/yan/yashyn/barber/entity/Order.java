package yan.yashyn.barber.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.sql.Date;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    private String surname;


    private String phoneNumber;

    @Email
    private String email;

    @Enumerated(EnumType.STRING)
    private StatusConsts status;

    @OneToOne
    private Barber barber;

   @OneToOne
    private Day day;

   @OneToOne
    private TimeTableForBarber timeTableForBarber;

   @OneToOne
    private Service service;
}
