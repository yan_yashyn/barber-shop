package yan.yashyn.barber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.request.ServiceRequest;
import yan.yashyn.barber.dto.response.ServiceResponse;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.RangRepository;
import yan.yashyn.barber.repository.ServiceRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceService {
    @Autowired
    public ServiceRepository serviceRepository;
    @Autowired
    public RangService rangService;

    public List<ServiceResponse> findByRangId(Long rangId) {
        return serviceRepository.findByRang_Id(rangId).stream()
                .map(ServiceResponse::new).collect(Collectors.toList());
    }

    public ServiceResponse addService(ServiceRequest serviceRequest) throws WrongInputException {
        yan.yashyn.barber.entity.Service service = new yan.yashyn.barber.entity.Service();
        service.setService(serviceRequest.getService());
        service.setDuration(serviceRequest.getDuration());
        service.setPrice(serviceRequest.getPrice());
        service.setRang(rangService.findOneById(serviceRequest.getRangId()));
        return new ServiceResponse(serviceRepository.save(service));
    }

    public yan.yashyn.barber.entity.Service findOneById(Long serviceId) throws WrongInputException {
        return serviceRepository.findById(serviceId).orElseThrow(() -> new WrongInputException("Day wasn't found"));
    }

//    public
}
