package yan.yashyn.barber.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
@AllArgsConstructor
public class DataResponse <T> {

    private List<T> content;

    private Integer totalPages;

    private Long totalElements;


}
