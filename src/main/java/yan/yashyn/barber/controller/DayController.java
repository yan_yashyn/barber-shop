package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.request.DayRequest;
import yan.yashyn.barber.dto.response.DayResponse;
import yan.yashyn.barber.service.DayService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/day")
public class DayController {

    @Autowired
    private DayService dayService;

    @PostMapping ("/set")
    private DayResponse set (@RequestBody DayRequest dayRequest){
        return dayService.set(dayRequest);
    }

    @GetMapping ("/get")
    private List<DayResponse> get (@RequestParam Long barberId) {return dayService.findByDayAfterAndBarberId(barberId);}

}
