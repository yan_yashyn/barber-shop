package yan.yashyn.barber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import yan.yashyn.barber.dto.response.TimeTableForBarberResponse;
import yan.yashyn.barber.entity.TimeTableForBarber;

import java.time.LocalTime;
import java.util.List;

@Repository
public interface TimeTableForBarberRepository extends JpaRepository <TimeTableForBarber, Long> {
    TimeTableForBarberResponse findByHourAndDay_Id (LocalTime hour, Long day_id);

    List<TimeTableForBarberResponse> findByDay_IdAndFreeHourEquals (Long day_id, Boolean freeHour);
}
