package yan.yashyn.barber.dto.request;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class ReviewsRequest {

    private String author;

    private String reviewMessage;

    private String postingTime;

    private Long barberId;

    private Integer mark;
}
