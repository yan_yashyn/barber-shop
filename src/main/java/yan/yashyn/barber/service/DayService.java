package yan.yashyn.barber.service;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.request.DayRequest;
import yan.yashyn.barber.dto.response.DayResponse;
import yan.yashyn.barber.dto.response.RangResponse;
import yan.yashyn.barber.entity.Day;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.BarberRepository;
import yan.yashyn.barber.repository.DayRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DayService {
    @Autowired
    private DayRepository dayRepository;
    @Autowired
    private BarberRepository barberRepository;

    public DayResponse set (DayRequest dayRequest){
        Day day = new Day();
        day.setBarber(barberRepository.getOne(dayRequest.getBarberId()));
        day.setDay(dayRequest.getDay());
        if (dayRepository.findByDayAndBarberId(dayRequest.getDay(), dayRequest.getBarberId()) == null){
            return new DayResponse(dayRepository.save(day));
        }
        else {
            return new DayResponse(dayRepository.findByDayAndBarberId(dayRequest.getDay(), dayRequest.getBarberId()));
        }
    }

    public List<DayResponse> findByDayAfterAndBarberId (Long barber_id){
//return dayRepository.findByDayAndBarber_Id(day, barber_id);
        LocalDate day = LocalDate.now();
        return dayRepository.findByDayAfterAndBarberId(day, barber_id).stream().map(DayResponse::new).collect(Collectors.toList());
    }

    public Day findOneById (Long dayId) throws WrongInputException {
        return dayRepository.findById(dayId).orElseThrow(() -> new WrongInputException("Day wasn't found"));
    }


}
