package yan.yashyn.barber.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RangRequest {

    private String rang;

    private Long serviceId;

}
