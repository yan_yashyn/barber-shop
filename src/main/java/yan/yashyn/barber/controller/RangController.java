package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.response.BarberResponse;
import yan.yashyn.barber.dto.response.RangResponse;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.service.RangService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/rang")
public class RangController {

    @Autowired
    private RangService rangService;

    @GetMapping("/findall")
    public List<RangResponse> getAllRangs () {
        return rangService.findAllRangs();
    }
}
