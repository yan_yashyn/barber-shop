package yan.yashyn.barber.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import yan.yashyn.barber.entity.Day;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class DayResponse {

    private Long id;

    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    private LocalDate date;

    private Long barberId;

    public DayResponse (Day day){
        id = day.getId();
        date = day.getDay();
    }
}
