package yan.yashyn.barber.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import yan.yashyn.barber.entity.Service;

import java.time.LocalTime;
@Getter
@Setter
@NoArgsConstructor
public class ServiceResponse {
    private Long id;

    private String serviceName;

    private Integer price;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "hh:mm")
    private LocalTime duration;

    public ServiceResponse (Service service){
        id = service.getId();
        serviceName = service.getService();
        price = service.getPrice();
        duration = service.getDuration();
    }
}
