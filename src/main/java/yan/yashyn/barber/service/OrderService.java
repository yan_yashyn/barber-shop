package yan.yashyn.barber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.request.OrderRequest;
import yan.yashyn.barber.dto.response.OrderResponse;
import yan.yashyn.barber.entity.Order;
import yan.yashyn.barber.entity.StatusConsts;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.OrderRepository;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private BarberService barberService;
    @Autowired
    private DayService dayService;
    @Autowired
    private TimeTableForBarberService timeTableForBarberService;
    @Autowired
    private ServiceService serviceService;

    public OrderResponse create(OrderRequest orderRequest) throws WrongInputException {
        Order order = new Order();
        order.setName(orderRequest.getName());
        order.setSurname(orderRequest.getSurname());
        order.setPhoneNumber(orderRequest.getPhoneNumber());
        order.setEmail(orderRequest.getEmail());
        order.setStatus(StatusConsts.WAITING);
        order.setBarber(barberService.findOneById(orderRequest.getBarberId()));
        order.setDay(dayService.findOneById(orderRequest.getDayId()));
        order.setTimeTableForBarber(timeTableForBarberService.findOneById(orderRequest.getTimeTableForBarberId()));
        order.setService(serviceService.findOneById(orderRequest.getServiceId()));
        return new OrderResponse(orderRepository.save(order));
    }
}
