package yan.yashyn.barber.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String Service;

    private Integer price;

    private LocalTime duration;

    @ManyToOne
    private Rang rang;

    @OneToOne (mappedBy = "service")
    private Order order;
}
