package yan.yashyn.barber.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import yan.yashyn.barber.entity.Rang;

@Getter
@Setter
@NoArgsConstructor
public class RangResponse {
    private Long id;

    private String rangName;

    public RangResponse (Rang rang){
        id = rang.getId();
        rangName = rang.getRang();
    }
}
