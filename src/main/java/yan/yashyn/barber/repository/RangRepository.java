package yan.yashyn.barber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import yan.yashyn.barber.entity.Rang;

import java.util.List;
import java.util.Optional;

@Repository
public interface RangRepository extends JpaRepository<Rang, Long> {


//    @Query ("from Rang r where r.rang like :rangParam")
//    List<Rang> byRang (@Param("rangParam") String a);

    Optional<Rang> findByRang (String rang);

    @Override
    Optional<Rang> findById(Long id);
}
