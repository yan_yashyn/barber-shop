package yan.yashyn.barber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import yan.yashyn.barber.repository.BarberRepository;
import yan.yashyn.barber.repository.OrderRepository;
import yan.yashyn.barber.repository.RangRepository;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class BarberApplication {

    @Autowired
    private RangRepository rangRepository;

    @Autowired
    private BarberRepository barberRepository;

    @Autowired
    private OrderRepository orderRepository;


    @PostConstruct
    public void init() {

    }

    public static void main(String[] args) {
        SpringApplication.run(BarberApplication.class, args);
    }

}
