package yan.yashyn.barber.dto.response;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import yan.yashyn.barber.entity.Barber;

import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
public class BarberResponse {

    private Long id;

    private String name;

    private String surname;

    private String rang;

    private String pathToImg;


    public BarberResponse (Barber barber){
        id = barber.getId();
        name = barber.getName();
        surname = barber.getSurname();
        if (barber.getRang() != null && barber.getRang() != null) {
            rang = barber.getRang().getRang();
        }
        pathToImg = barber.getPathToImg();
    }
}
