package yan.yashyn.barber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import yan.yashyn.barber.entity.Reviews;

import java.util.List;



@Repository
public interface ReviewsRepository extends JpaRepository<Reviews, Long> {
    List<Reviews> findAllByBarberId(Long barberId);
}
