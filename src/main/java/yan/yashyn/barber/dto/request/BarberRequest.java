package yan.yashyn.barber.dto.request;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter @Setter
public class BarberRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    @NotNull
    private Long rangId;

    private FileRequest fileRequest;
}
