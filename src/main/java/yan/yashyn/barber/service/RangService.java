package yan.yashyn.barber.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.response.RangResponse;
import yan.yashyn.barber.entity.Rang;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.RangRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RangService {

    @Autowired
    private RangRepository rangRepository;

    public Rang findOneByRang (String rang) throws WrongInputException {
    return rangRepository.findByRang(rang).orElseThrow(() -> new WrongInputException("Rang wasn't found"));
    }

    public Rang findOneById (Long id) throws WrongInputException {
        return rangRepository.findById(id).orElseThrow(() -> new WrongInputException("Rang wasn't found"));
    }

    public List<RangResponse> findAllRangs (){
        return rangRepository.findAll().stream().map(RangResponse::new).collect(Collectors.toList());
    }
 }
