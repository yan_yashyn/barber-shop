package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.request.ReviewsRequest;
import yan.yashyn.barber.dto.response.ReviewsResponse;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.service.BarberService;
import yan.yashyn.barber.service.ReviewsService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/reviews")
public class ReviewsController {
    @Autowired
    private ReviewsService reviewsService;

    @Autowired
    private BarberService barberService;

    @PostMapping("/save")
    public ReviewsResponse save (@RequestBody ReviewsRequest reviewsRequest) throws WrongInputException {
        return reviewsService.save(reviewsRequest);
    }

    @GetMapping("/display")
    public List<ReviewsResponse> displayReviews (@RequestParam Long barberId){
        return reviewsService.findAllByBarberId(barberId);
    }
}
