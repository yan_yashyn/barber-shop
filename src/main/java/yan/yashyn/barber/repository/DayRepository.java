package yan.yashyn.barber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import yan.yashyn.barber.dto.response.DayResponse;
import yan.yashyn.barber.entity.Day;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DayRepository extends JpaRepository<Day, Long> {



//    @Query(
//            value = "FROM Day d WHERE d.day > :start and d.barber_id = ?2")
//    List <Day> findByDayAfterAndBarber_Id (@Param("start") LocalDate day, Long barber_id);

    List<Day> findByDayAfterAndBarberId(LocalDate day, Long barberId);

    Day findByDayAndBarberId(LocalDate day, Long barberId);
}
