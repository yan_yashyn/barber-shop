package yan.yashyn.barber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import yan.yashyn.barber.dto.response.BarberResponse;
import yan.yashyn.barber.entity.Barber;

import java.util.List;

@Repository
public interface BarberRepository extends JpaRepository <Barber, Long> {

    List<Barber> findByRang_Id (Long rang_id);
}
