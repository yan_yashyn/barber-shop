package yan.yashyn.barber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.request.BarberRequest;
import yan.yashyn.barber.dto.response.BarberResponse;
import yan.yashyn.barber.dto.response.DataResponse;
import yan.yashyn.barber.entity.Barber;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.BarberRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BarberService {

    @Autowired
    private BarberRepository barberRepository;

    @Autowired
    private RangService rangService;

    @Autowired
    private FileService fileService;

    public BarberResponse save (BarberRequest barberRequest) throws WrongInputException, IOException {
        return new BarberResponse(barberRepository.save(barberRequestToBarber(barberRequest, null)));
    }

    public List<BarberResponse> findAll() {
        return barberRepository.findAll().stream()
                .map(BarberResponse::new).collect(Collectors.toList());
    }

    public void delete(Long id) {
        barberRepository.deleteById(id);
    }

    public BarberResponse update (Long id, BarberRequest barberRequest) throws WrongInputException, IOException {
        return new BarberResponse(barberRepository
                .save(barberRequestToBarber(barberRequest, findOneById(id))));
    }

    public Barber findOneById (Long id) throws WrongInputException {
        return barberRepository.findById(id).orElseThrow(() -> new WrongInputException("Barber wasn't found"));
    }

    public BarberResponse findBarberById (Long barberId){
        return new BarberResponse (barberRepository.getOne(barberId));
    }
    private Barber barberRequestToBarber (BarberRequest barberRequest, Barber barber) throws WrongInputException, IOException {
        if (barber == null){
            barber = new Barber();
        }
        barber.setName(barberRequest.getName());
        barber.setSurname(barberRequest.getSurname());
        barber.setRang(rangService.findOneById(barberRequest.getRangId()));
        if (barberRequest.getFileRequest() == null){
        String file = fileService.saveFile(barberRequest.getFileRequest());
        barber.setPathToImg(file);
        }
        return barber;
    }

    public DataResponse <BarberResponse> findAll (Integer page, Integer size, String fieldName, Sort.Direction direction){
        if (direction == null){
            direction = Sort.Direction.ASC;
        }
        if (fieldName == null){
            fieldName = "id";
        }
        Page<Barber> barberPage = barberRepository.findAll(PageRequest.of(page, size, direction, fieldName));
        return new DataResponse<>(
                barberPage.get().map(BarberResponse::new).collect(Collectors.toList()),
                barberPage.getTotalPages(),
                barberPage.getTotalElements());
    }

    public List<BarberResponse> findByRangId(Long rangId) {
        return barberRepository.findByRang_Id(rangId).stream()
                .map(BarberResponse::new).collect(Collectors.toList());
    }
}
