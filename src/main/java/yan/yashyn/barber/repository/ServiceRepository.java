package yan.yashyn.barber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import yan.yashyn.barber.dto.response.ServiceResponse;
import yan.yashyn.barber.entity.Service;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {
    List<Service> findByRang_Id (Long rang_id);
}
