package yan.yashyn.barber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yan.yashyn.barber.dto.request.ReviewsRequest;
import yan.yashyn.barber.dto.response.ReviewsResponse;
import yan.yashyn.barber.entity.Reviews;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.repository.ReviewsRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewsService {
    @Autowired
    private ReviewsRepository reviewsRepository;
    @Autowired
    private BarberService barberService;

    public ReviewsResponse save (ReviewsRequest reviewsRequest) throws WrongInputException {
    Reviews reviews = new Reviews();
    reviews.setAuthor(reviewsRequest.getAuthor());
    reviews.setReviewMessage(reviewsRequest.getReviewMessage());
    reviews.setPostingTime(reviewsRequest.getPostingTime());
    reviews.setBarber(barberService.findOneById(reviewsRequest.getBarberId()));
    reviews.setMark(reviewsRequest.getMark());
    return new ReviewsResponse(reviewsRepository.save(reviews));
    }

    public List <ReviewsResponse> findAllByBarberId (Long barberId){
        return reviewsRepository.findAllByBarberId(barberId).stream().map(ReviewsResponse::new)
                .collect(Collectors.toList());
    }
}
