package yan.yashyn.barber.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Barber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String surname;

    private String pathToImg;

    @ManyToOne
    private Rang rang;

    @OneToOne (mappedBy = "barber")
    private Order order;

    @OneToMany (mappedBy = "barber", orphanRemoval = true)
    private List<Day> days = new ArrayList<>();

    @OneToMany (mappedBy = "barber")
    private List <Reviews> reviewsList = new ArrayList<>();
}
