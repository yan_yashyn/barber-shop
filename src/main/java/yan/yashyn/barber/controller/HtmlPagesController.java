package yan.yashyn.barber.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HtmlPagesController {


    @RequestMapping("/")
    public String contactPage (){
        return "main.html";
    }
}
