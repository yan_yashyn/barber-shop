package yan.yashyn.barber.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import yan.yashyn.barber.entity.Reviews;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ReviewsResponse {
    private Long id;

    private String author;

    private String reviewMessage;

    private String postingTime;

    private Integer mark;

    private String barberName;

    public ReviewsResponse (Reviews reviews){
        id = reviews.getId();
        author = reviews.getAuthor();
        reviewMessage = reviews.getReviewMessage();
        postingTime = reviews.getPostingTime();
        barberName = reviews.getBarber().getName();
        mark = reviews.getMark();
    }
}
