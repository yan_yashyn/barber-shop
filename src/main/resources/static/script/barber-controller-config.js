var mainUrl = "http://localhost:8080";

setActionOnCreateButton();
setActionOnTimeTableButton();
setActionOnServiceButton();
getAllBarbers();


function setActionOnCreateButton() {

    function getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    $("#createBarber").click(function () {
        var file = document.getElementById("photo").files[0];
        var name = $('#name').val();
        var surname = $('#surname').val();
        var rang = $('#rang').val();
        var newBarber = {};


        if (file != null){
        getBase64(file).then(data => {
             newBarber = {
                "fileRequest": {
                    "data": data,
                    "fileName": name
                },
                "name": name,
                "rangId": rang,
                "surname": surname
            };
        });
        }else {
            newBarber = {
                "fileRequest": {
                    "data": null,
                    "fileName": null
                },
                "name": name,
                "rangId": rang,
                "surname": surname
            };
        }

            $.ajax({
                url: mainUrl + "/barber/save",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(newBarber),
                success: function (data) {
                    location.reload();
                    alert("Барбер створений");
                },
                error: function (error) {
                    alert(error.message);
                }
            });
        });
}

function getAllBarbers() {
    $.ajax({
        url: mainUrl + "/barber/allbarbers",
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            setBarbersToTable(dataResponse);
            setActionOnDeleteButtons();
            setSelectBox(dataResponse);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setBarbersToTable(barbers) {
    let tableOfBarbers = $("#barbers");
    for (let barber of barbers) {
        tableOfBarbers.append('<tr>' +
            '<td>' + barber.name + '</td>' +
            '<td>' + barber.surname + '</td>' +
            '<td>' + barber.rang + '</td>' +
            '<td><button class="button" value="' + barber.id + '">Delete</button></td>' +
            '</tr>');
    }
}


function setActionOnDeleteButtons() {
    $(".button").each(function (index) {
        $(this).click(function () {
            $.ajax({
                url: mainUrl + "/barber/delete?id=" + $(this).val(),
                type: "DELETE",
                success: function (data) {
                    alert("Барбер був видалений")
                    location.reload();
                },
                error: function (error) {
                    alert(error.message);
                }
            });
        })
    });
}


function setSelectBox(barbers) {
    let select = $("#barbers-select");
    for (let barber of barbers) {
        select.append(
            '<option value="' + barber.id + '">' + barber.name + " " + barber.surname + '</option>'
        );
    }
}


function setActionOnTimeTableButton() {
    $(".submit-timetable").click(function () {
        var barberId = $('#barbers-select').val();
        var hours = [];
        $.each($("input[name='hour']:checked"), function () {
            hours.push($(this).val());
        });
        var day = $('#day').val();


        var makeday ={
            "barberId": barberId,
            "day": day,
        };
        makeDay(makeday, hours);
    });
}

function postTimeTable(response, hours) {
    for (var i = 0; i < hours.length; i++) {
        var newTimeTable = {
            "dateId": response.id,
            "freeHour": true,
            "time": hours[i]
        };
       post(newTimeTable);
    }
}

function makeDay(makeday, hours) {
$.ajax({
        url: mainUrl + "/day/set",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(makeday),
        success: function (response) {
            postTimeTable (response, hours)
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function post(newTimeTable) {
    $.ajax({
        url: mainUrl + "/ttforbarber/set",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(newTimeTable),
        success: function (data) {

        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function setActionOnServiceButton(){
    $(".submit-service").click(function () {
        let duration = $('#serviceDuration').val();
        let price = $('#servicePrice').val();
        let rangId = $('#serviceRang').val();
        let service = $('#serviceName').val();

        let newService = {
            "duration": duration,
            "price": price,
            "rangId": rangId,
            "service": service
        }

        $.ajax({
            url: mainUrl + "/service/addservice",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(newService),
            success: function (data) {
                location.reload();
            },
            error: function (error) {
                alert(error);
            }
        });
    });
}