package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.request.BarberRequest;
import yan.yashyn.barber.dto.response.BarberResponse;
import yan.yashyn.barber.dto.response.DataResponse;
import yan.yashyn.barber.entity.Barber;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.service.BarberService;

import java.io.IOException;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/barber")
public class BarberController {

@Autowired
    private BarberService barberService;

    @PostMapping ("/save")
    public BarberResponse save (@RequestBody BarberRequest barberRequest) throws WrongInputException, IOException {
        return barberService.save(barberRequest);
    }

    @GetMapping ("/findone")
    public BarberResponse findOneById (@RequestParam Long barberId) throws WrongInputException {
        return barberService.findBarberById(barberId);
    }

    @GetMapping("/allbarbers")
    public List<BarberResponse> findAll (){
        return barberService.findAll();
    }


    @DeleteMapping("/delete")
    public void delete (@RequestParam Long id){
        barberService.delete(id);
    }

    @PutMapping("/update")
    public BarberResponse update (@RequestParam Long id, @RequestParam BarberRequest barberRequest) throws WrongInputException, IOException {
        return barberService.update(id, barberRequest);
    }

    @GetMapping ("/byrang")
    public List<BarberResponse> findByRangId (@RequestParam Long rangId){
        return barberService.findByRangId(rangId);
    }

    @GetMapping ("/page")
    public DataResponse<BarberResponse> findAll (@RequestParam Integer page,
                                                 @RequestParam Integer size,
                                                 @RequestParam (required = false) Sort.Direction direction,
                                                 @RequestParam (required = false) String fieldName){
        return barberService.findAll(page, size, fieldName, direction);
    }
}
