package yan.yashyn.barber.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class TimeTableForBarber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean freeHour;

    private LocalTime hour;

    @ManyToOne
    private Day day;

    @OneToOne(mappedBy = "timeTableForBarber")
    private Order order;
}
