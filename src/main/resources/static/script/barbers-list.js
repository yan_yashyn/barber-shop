var mainUrl = "http://localhost:8080";
var $reviewsContainer = $('.reviews-container');

getAllBarbers();


function getAllBarbers() {
    $.ajax({
        url: mainUrl + "/barber/allbarbers",
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
            appendAllBarbers(dataResponse);
            setReviewsModalConfiguration();
            setActionOnReviewsButtons();
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function appendAllBarbers(barbers) {
    let $container = $('.barber-container');
    for (let barber of barbers) {
        let img = barber.pathToImg ?
            'http://localhost:8080/img/' + barber.pathToImg
            : 'https://wingslax.com/wp-content/uploads/2017/12/no-image-available.png';
        $container.append(`
     <div class="barber" style="display: inline-block">
    <img class="barber-img" src="${img}" alt="" style="">
    <p class="barber-name">
    ${barber.name}
</p>
<p class="barber-surname">
${barber.surname}
</p>
    <p class="barber-rang">
    ${barber.rang}
</p>
<button class="reviewsBtn" value="${barber.id}">Відгуки</button>
</div>

     `)

    }
}

function setReviewsModalConfiguration() {
    // Get the modal
    let modal = $('.reviewsWindow');

    // Get the button that opens the modal
    let btn = $('.reviewsBtn');

    // Get the <span> element that closes the modal
    let span = document.getElementsByClassName("close")[0];


    // When the user clicks the button, open the modal
    btn.click(function () {
        modal[0].style.display = "block";
        $reviewsContainer.html("");
    });

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal[0].style.display = "none";
        $reviewsContainer.html("");
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal[0].style.display = "none";
        }
    };
}

function setActionOnReviewsButtons() {
    $(".reviewsBtn").each(function (index) {
        $(this).click(function () {
            getBarberName($(this).val());
            setActionOnSendButton($(this).val());
        })
    });
}

function getBarberName(barberId) {
    $.ajax({
        url: mainUrl + "/barber/findone?barberId=" + barberId,
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
        addNameToHeader(dataResponse)
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function addNameToHeader(barber) {
    $reviewsContainer.append(`    
       <p class="barber-name">
   Відгуки про барбера: ${barber.name}
</p>
    `);
    getReviews(barber.id);
}

function getReviews(barberId) {
    $.ajax({
        url: mainUrl + "/reviews/display?barberId=" + barberId,
        type: "GET",
        contentType: "application/json",
        success: function (dataResponse) {
        viewReviews(dataResponse);
        },
        error: function (error) {
            console.log(error);
        }
    });
}




function viewReviews(reviews) {
    let reviewMark = 0;
    let counter = 0;
    for (let review of reviews){
        reviewMark = reviewMark + review.mark;
        counter++;
    }
    let mark = 0;

    if (counter !== 0){
    mark = reviewMark/counter;
    mark = mark.toFixed(1);
    }

    $reviewsContainer.append(`
    <p class="totalMark"> Середня оцінка: ${mark}/5 (Кількість відгуків ${counter})</p>  
    `);

    for (let review of reviews) {
        $reviewsContainer.append(`
     <div class="review">
    <p class="author-name">
   <b>Автор:</b> ${review.author}
</p>
<p class="send-date">
<b>Дата публікації:</b> ${review.postingTime}
</p>
<p class="review-mark">
<b>Оцінка:</b> ${review.mark}
</p>
    <p class="review-message">
    ${review.reviewMessage}
</p>
<hr>
</div>
`);
    }
}


function setActionOnSendButton (barberId){
$("#send").click(function () {
    var author = $('#author-name').val().trim();

    if (author === ""){
        author = "Анонімний користувач"
    }

    var time = new Date();
    var date = time.getDate();
    var month = time.getMonth();
    var year = time.getFullYear();

    var monthName;

    if (date < 10){
        date = "0"+date;
    }
    switch (month) {
        case 0:
            monthName="січня";
            break;
        case 1:
            monthName="лютого";
            break;
        case 2:
            monthName="березня";
            break;
        case 3:
            monthName="квітня";
            break;
        case 4:
            monthName="травня";
            break;
        case 5:
            monthName="червня";
            break;
        case 6:
            monthName="липня";
            break;
        case 7:
            monthName="серпня";
            break;
        case 8:
            monthName="вересня";
            break;
        case 9:
            monthName="жовтня";
            break;
        case 10:
            monthName="листопада";
            break;
        case 11:
            monthName="грудня";
            break;
    }

    var dateString = date + " " + monthName + " " + year;
    var message = $('#review-message').val();
    var mark = $("input[name=reviewStars]:checked").val();


    var newReview = {
        "author": author,
        "barberId": barberId,
        "mark": mark,
        "postingTime": dateString,
        "reviewMessage": message
    };

   $.ajax({
        url: mainUrl + "/reviews/save",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(newReview),
        success: function (data) {
            location.reload();
            alert("Відгук був створений. Дякуемо!");
        },
        error: function (error) {
            alert(message.error);
        }
    });
});
}