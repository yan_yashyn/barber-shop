package yan.yashyn.barber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yan.yashyn.barber.dto.request.ServiceRequest;
import yan.yashyn.barber.dto.response.ServiceResponse;
import yan.yashyn.barber.exception.WrongInputException;
import yan.yashyn.barber.service.ServiceService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/service")
public class ServiceController {
    @Autowired
    private ServiceService serviceService;

    @GetMapping ("/byrang")
    public List<ServiceResponse> getByRang (@RequestParam Long rangId){
        return serviceService.findByRangId(rangId);
    }

    @PostMapping ("/addservice")
    public ServiceResponse addService (@RequestBody ServiceRequest serviceRequest) throws WrongInputException {
        return serviceService.addService(serviceRequest);
    }
}
