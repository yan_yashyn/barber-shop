package yan.yashyn.barber.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import yan.yashyn.barber.entity.TimeTableForBarber;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class TimeTableForBarberResponse {

    private Long id;

    private Boolean freeHour;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime time;

    public TimeTableForBarberResponse (TimeTableForBarber timeTableForBarber){
        id = timeTableForBarber.getId();
        freeHour = timeTableForBarber.getFreeHour();
        time = timeTableForBarber.getHour();
    }
}
